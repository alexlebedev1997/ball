#Инициализация библиотек
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import animation
matplotlib.use("Agg")
from matplotlib.animation import FFMpegWriter
from IPython.display import HTML
def anim_moving_oneball(x,y):
#plot creating
    fig,ax=plt.subplots(figsize=(5,5))
#object creating with empty parametres
    oneball,=ax.plot([],[],'r.',ms=30)
#frame updating
    def updatefig(frame):
        oneball.set_xdata(x[frame])
        oneball.set_ydata(y[frame])
        return oneball,
    def init():
        ax.set_xlim(-10,10)
        ax.set_ylim(-1,10)
        oneball.set_xdata(x[0])
        oneball.set_ydata(y[0])
        return oneball,
#creating of anumation object
    anim=animation.FuncAnimation(fig, updatefig, np.arange(len(x)), init_func=init, interval=50,
        blit=True,repeat=False)
    plt.close(fig)
    return anim
#inicialization of parametres
def under_gravity(x0,y0,vx0,vy0,ax,ay,tmax,dt):
    nt=int(tmax/dt)#number of time steps
    t=np.linspace(0,tmax, nt)#massive for time
    x=np.zeros(t.size)#massive for x
    y=np.zeros(t.size)#massive for y
    x[0]=x0#initial coordinate on x
    y[0]=y0#initial coordinate on y
    vx=vx0#initial velocity on x
    vy=vy0#initial velocity on y
#motion of one ball
    for i in range(1,t.size-1):
        x[i]=x[i-1]+vx*dt+ax*dt**(2)/2#equation of motion on x
        y[i]=y[i-1]+vy*dt+ay*dt**(2)/2#equation of motion on y
        vx=vx+ax*dt#velocity on x
        vy=vy+ay*dt#velocity on y
        #reflection of ball
        for j in range(0,t.size):
            if y[j]<0:
                vy=-vy#reflection for velocity  on y
            if x[j]>10 or x[j]<-10:
                vx=-vx#reflection for velocity on x
    return x,y
x,y=under_gravity(5,7,5,0,0,-10,15,0.05)
#print(x,y)
#creating animation
anima=anim_moving_oneball(x,y)
HTML(anima.to_html5_video())
FFwriter=FFMpegWriter(fps=15, metadata=dict(artist='Me'), bitrate=1800)
anima.save('oneball.mp4', writer=FFwriter)
