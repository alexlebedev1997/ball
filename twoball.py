#Инициализация библиотек
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import animation
matplotlib.use("Agg")
from matplotlib.animation import FFMpegWriter
from IPython.display import HTML
def anim_moving_oneball(x,y):
#plot creating
    fig,ax=plt.subplots(figsize=(5,5))
#object creating with empty parametres
    oneball,=ax.plot([],[],'r.',ms=30)
    oneball1,=ax.plot([],[],'b.',ms=30)
    
#frame updating
    def updatefig(frame):
        oneball.set_xdata(x1[frame])
        oneball.set_ydata(y1[frame])
        oneball1.set_xdata(x2[frame])
        oneball1.set_ydata(y2[frame])
        return oneball,
        return oneball1,
    def init():
        ax.set_xlim(-10,10)
        ax.set_ylim(-1,10)
        oneball.set_xdata(x1[0])
        oneball.set_ydata(y1[0])
        ax.set_xlim(-10,10)
        ax.set_ylim(-1,10)
        oneball1.set_xdata(x2[0])
        oneball1.set_ydata(y2[0])
        return oneball,
        return oneball1,
    #def updatefig(frame):
        
    
    #def init():
        
#creating of anumation object
    anim=animation.FuncAnimation(fig, updatefig, np.arange(len(x)), init_func=init, interval=50,
        blit=True,repeat=False)
    
    plt.close(fig)
    return anim
    #anim=animation.FuncAnimation(fig, updatefig, np.arange(len(x)), init_func=init, interval=50,
        #blit=True,repeat=False)
    
    #plt.close(fig)
    #return anim
#inicialization of parametres
def under_gravity(x10,y10,vx10,vy10,ax1,ay1,tmax1,dt1):
    nt1=int(tmax1/dt1)#number of time steps
    t1=np.linspace(0,tmax1, nt1)#massive for time
    x1=np.zeros(t1.size)#massive for x
    y1=np.zeros(t1.size)#massive for y
    x1[0]=x10#initial coordinate on x
    y1[0]=y10#initial coordinate on y
    vx1=vx10#initial velocity on x
    vy1=vy10#initial velocity on y
#motion of one ball
    for i in range(1,t1.size-1):
        x1[i]=x1[i-1]+vx1*dt1+ax1*dt1**(2)/2#equation of motion on x
        y1[i]=y1[i-1]+vy1*dt1+ay1*dt1**(2)/2#equation of motion on y
        vx1=vx1+ax1*dt1#velocity on x
        vy1=vy1+ay1*dt1#velocity on y
        #reflection of ball
        for j in range(0,t1.size):
            if y1[j]<0:
                vy1=-vy1#reflection for velocity  on y
            if x1[j]>10 or x1[j]<-10:
                vx1=-vx1#reflection for velocity on x
    return x1,y1
x1,y1=under_gravity(6,8,5,0,0,-10,29,0.05)


x2,y2=under_gravity(5,4,17,0,0,-10,29,0.05)

#creating animation
anima=anim_moving_oneball(x1,y1)
anima=anim_moving_oneball(x2,y2)
HTML(anima.to_html5_video())
FFwriter=FFMpegWriter(fps=15, metadata=dict(artist='Me'), bitrate=1800)
anima.save('twobal1s.mp4', writer=FFwriter)
